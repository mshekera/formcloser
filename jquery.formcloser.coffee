$ = jQuery

$.event.special.destroying = {
    remove: (o) ->
        o.handler?(@, o.data)
}

$.fn.extend {
    'formcloser' : (options) ->
        @each () ->
            $this = $ @

            formcloser = $this.data 'formcloser'

            if formcloser and typeof options is 'string'
                switch options
                    when 'destroy' then formcloser.destroy()
                    when 'close' then formcloser.close()
            else if not formcloser
                $this.data 'formcloser', new Formcloser(@, options)
}

class AbstractFormcloser
    constructor: ->
        @options = {
            'show_event' : 'show'
            'hide_event' : 'hide'
            'namespace' : 'formcloser'
            'close_message' : 'Highlighted forms edited but not saved. Procceed?'
        }
        @options['edited_name'] = @options['namespace']+'_edited'
        @options['highlighter_class'] = @options['namespace']+'-highlight'

class Formcloser extends AbstractFormcloser
    constructor: (container, options) ->
        super()
        @container = $(container)
        @forms = @container.find 'form'
        @isEdited = false
        @openedForms = 0
        @register_listeners()

        if typeof options is 'object'
            for own attr, val of options
                @options[attr] = val

    register_extend: (evt, namespace) ->
        _org = $.fn[evt]
        $.fn[evt] = () ->
            @trigger(if namespace then namespace+'.'+evt else evt)
            _org.apply this, arguments

    on_change_listener: (ev) ->
        that = ev.data
        if not $(@).closest('form').data that.options['edited_name']
            $(@).closest('form').data that.options['edited_name'], true
            ++that.openedForms

    on_submit_listener: (ev) ->
        that = ev.data
        $this = $(@)
        if $this.data that.options['edited_name']
            if typeof $this.valid isnt 'function' or $this.valid()
                $this.data that.options['edited_name'], false
                --that.openedForms

    on_remove_listener: (form, that) ->
        if $(form).data that.options['edited_name']
            --that.openedForms
            $(@).data that.options['edited_name'], false

    on_hide_event: (ev) ->
        that = ev.data
        if $(@).data that.options['edited_name']
            --that.openedForms

    on_show_event: (ev) ->
        that = ev.data
        if $(@).data that.options['edited_name']
            ++that.openedForms

    register_listeners: ->
        that = @
        @forms.on 'change', 'button, input, textarea, select', that, @on_change_listener

        @forms.on 'submit', that, @on_submit_listener

        @forms.on 'destroying', that, @on_remove_listener

        if @options['on_hide_event'] is 'hide'
            @register_extend @options['on_hide_event'], @options['namespace']
            event = @options['namespace']+'.'+@options['on_hide_event']
        else
            event = @options['on_hide_event']
        
        @forms.on event, that, @on_hide_event

        if @options['on_show_event'] is 'show'
            @register_extend @options['on_show_event'], @options['namespace']
            event = @options['namespace']+'.'+@options['on_show_event']
        else
            event = @options['on_show_event']

        @forms.on event, that, @on_show_event

    close: ->
        if @openedForms isnt 0
            hidden = 0
            that = @
            @forms.each ->
                $form = $(@)
                if not $form.is(':visible') and $form.data that.options['edited_name']
                    ++hidden

            if @openedForms - hidden isnt 0
                @forms.each ->
                    $form = $(@)
                    if $form.data that.options['edited_name']
                        $form.removeClass that.options['highlighter_class']
                        $form.css 'display'
                        $form.addClass that.options['highlighter_class']

                if not confirm @options['close_message']
                    return

        @container.remove()
        @destroy()

    destroy: ->
        console.log 'destroying...'
        @forms.off 'change', 'button, input, textarea, select', @on_change_listener

        @forms.off 'submit', @on_submit_listener

        @forms.off 'destroying', @on_remove_listener
        
        if @options['on_hide_event'] is 'hide'
            event = @options['namespace']+'.'+@options['on_hide_event']
        else
            event = @options['on_hide_event']
        
        @forms.off event, @on_hide_event

        if @options['on_show_event'] is 'show'
            event = @options['namespace']+'.'+@options['on_show_event']
        else
            event = @options['on_show_event']

        @forms.off event, @on_show_event